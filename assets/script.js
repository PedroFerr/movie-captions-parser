// ==============================================================================
// Stolen from here https://stackoverflow.com/questions/33145762/parse-a-srt-file-with-jquery-javascript/33147421
// Adapted here, by PF @ http://www.Miles-NET.com/cv, to André Dias
// ==============================================================================
const PF_SRT = function () {
    const pattern = /(\d+)\n([\d:,]+)\s+-{2}\>\s+([\d:,]+)\n([\s\S]*?(?=\n{2}|$))/gm;

    const parse = function (f) {
        if (typeof (f) != 'string')
            throw 'Sorry, Parser accept string only.';

        const result = [];
        if (f == null)
            return _subtitles;

        f = f.replace(/\r\n|\r|\n/g, '\n');

        while ((matches = pattern.exec(f)) != null) {
            result.push(toLineObj(matches));
        }
        return result;
    }

    const toLineObj = function (group) {
        return {
            line: group[1],
            startTime: group[2],
            endTime: group[3],
            text: group[4]
        };
    }

    return { parse: parse }
}();

$(function () {

    // Listeners:
    $('.table-filter.btn-group > button').on('click', function(evt) {
        const thisBtn = evt.target;        
        const tableFilterBtnGroup = document.querySelectorAll('.table-filter.btn-group > button');

        tableFilterBtnGroup.forEach(btn => btn.classList.remove('active'));
        thisBtn.classList.add('active');
    });

    $('#parseFromTextArea').on('click', function () {
        try {
            const text = $('#source').val();
            const result = PF_SRT.parse(text);
            // console.log(result);

            const wrapper = $('#result tbody');
            wrapper.html('');

            for (const line in result) {
                const obj = result[line];
                wrapper.append(`
                    <tr>
                        <td class="line-idx">${obj.line}</td>
                        <td>${obj.startTime}</td>
                        <td>${obj.endTime}</td>
                        <td class="caption">${obj.text}</td>
                        <td class="translated-caption">&nbsp;</td>
                    </tr>
                `);
            }

            initConditionsAfterParsing(result.length);
        
        } catch (e) {
            console.error(e);
        }
    });

    // Init 'onLoad' DOM's condiitons:
    $('#parseFromTextArea').click();

});
// ==============================================================================

initConditionsAfterParsing = (nLines) => {

    // 1)
    const filterElementsTagHTML = `<i class="fa fa-filter text-primary" aria-hidden="true"></i>: ` +
        `<span id="nElements">${nLines}</span>/`+
        `<span id="totalElements" class="font-weight-bold">${nLines}</span> <i class="text-primary">lines</i>`
    ;
    document.querySelector('#filterResults').innerHTML = filterElementsTagHTML;
    // 2)
    const tableFilterBtnGroup = document.querySelectorAll('.table-filter.btn-group > button');
    tableFilterBtnGroup.forEach((btn, idx) => btn.setAttribute('disabled', 'true'));
    // 3)
    document.querySelector('button#saveFromParser').setAttribute('disabled', 'true');
}

loadFromFile = (inputElement, validExtension) => {
    // Check user is not cheating or getting confused - there's a lot of 'files' that are 'any' typed as '.*' (i.e. folders and shotcut items):
    const fileName = inputElement.value;
    const fileExt = fileName.substring(fileName.lastIndexOf('.'));    

    if(fileName) {
        if (fileExt !== validExtension) {
            window.alert(`Invalid "${fileName}" file selected\n\nValid files are only those of\n "${validExtension}"\n type.\n\nPlease choose a valid file...`);
            return false;
        }
    } else {
        return false;
    }

    toggleLoadingWheel();

    /**
     * Do the to-call File Reader Method,
     * which returns a Promise of either everything is OK or an Error ocurred
     *
     * @param {*} chosenFile - the file inputted on an HTMLElement
     * @returns Promise
     */
    const readFileContent = (chosenFile) => {
        const reader = new FileReader();
        // Start reading it, if it's a binary or blob:
        // reader.readAsText(this.selectedFile);
        // reader.readAsArrayBuffer(blob);
        // reader.readAsBinaryString(blob)
        // reader.readAsDataURL(blob)

        return new Promise((resolve, reject) => {
            // reader.readAsText(chosenFile, 'ISO-8859-1');
            // reader.readAsText(chosenFile, 'UTF-8');
            reader.readAsText(chosenFile);

            reader.onloadend = evt => resolve(evt.target.result);
            reader.onerror = error => reject(error);
            reader.onabort = () => {
                const msg = 'File upload was aborted by the user...';
                console.warn(msg)
                reject(msg);
            };
        })
    
    }
    
    // All good:
    readFileContent(inputElement.files[0])
        .then(content => {
            // console.log(content);
            toggleLoadingWheel();
            // Allow the user to pick up the SAME file => we need to induce a CHANGE:
            inputElement.value = '';

            if(validExtension === '.srt') {
                document.getElementById('source').value = content;
                if (window.confirm('Parse it right now...?')) { $('#parseFromTextArea').click(); }
            }
            if (validExtension === '.txt'){
                replaceLinesByTranslations(content);
            }
        })
        .catch(error => console.log(error))
    ;
}

replaceLinesByTranslations = (content) => {
    const arrayOfTranslationLines = content.replace(/\r/g, '').split(/\n/);
    const translatedNRows = arrayOfTranslationLines.length;

    const currentCaptionCols = document.querySelectorAll('#result td.translated-caption');
    const currentNRows = currentCaptionCols.length;

    // console.error(arrayOfTranslationLines, currentCaptionCols);

    if (currentNRows !== translatedNRows) {
        const confirmMsg = `Current '.srt' file lines (${currentNRows}) are` +
            `\nDIFFERENT\n`+
            `than the number of lines (${translatedNRows}) of the uploaded '.txt', with translations\n\n` +
            `Are you sure you\nSTILL wnant to REPLACE\nprevious caption lines by those ones...?`
        ;
        if (window.confirm(confirmMsg)) {
            doTranslateEachLine(currentCaptionCols, arrayOfTranslationLines);
        }
    } else {
        doTranslateEachLine(currentCaptionCols, arrayOfTranslationLines);
    }
}

doTranslateEachLine = (currentCaptionCols, arrayOfTranslationLines) => {

    initConditionsBeforeTranslation(currentCaptionCols);

    for (let i = 0; i < arrayOfTranslationLines.length; i++) {
        const line = arrayOfTranslationLines[i];
        // if (i < 4) { console.warn(line); }

        if (currentCaptionCols[i]) {
            currentCaptionCols[i].textContent = line;

            const thisRow = currentCaptionCols[i].parentElement;
            const oldCaption = thisRow ? thisRow.querySelector('td.caption') : null;
            
            // Signal the ones that are exactly the same, from the others
            if (oldCaption) {
                const signal = thisRow.children[0];                    
                if(signal) {
                    const signalClass = (oldCaption.textContent === currentCaptionCols[i].textContent) ?
                        'is-green'
                        :
                        'is-red'
                    ;
                    signal.classList.add(signalClass);
                    // <tr> of this row's <td> should also be classed the same, NOT for styling reasons but for JQuey DOM manipulation reasons:
                    signal.parentElement.classList.add(signalClass);
                }
            }                    
        }

        // Finished? The DOM manipulation...?
        if (i === arrayOfTranslationLines.length - 1) {
            setTimeout(() => addTranslationFiltersListeners(), 0);
        }
    }
}

initConditionsBeforeTranslation = (currentCaptionCols) => {

    document.querySelector('#totalElements').innerHTML = currentCaptionCols.length;

    currentCaptionCols.forEach(col => col.innerHTML = '&nbsp;')

    document.querySelectorAll('#result > tbody > tr').forEach(row => {
        row.classList.remove('is-green');
        row.children[0].classList.remove('is-green');
        
        row.classList.remove('is-red');
        row.children[0].classList.remove('is-red');
    });
}

addTranslationFiltersListeners = () => {
    document.querySelector('button#saveFromParser').removeAttribute('disabled');

    const $tableGreens = $('#result > tbody > tr.is-green');
    const greensLength = $tableGreens.length;

    const $tableReds = $('#result > tbody > tr.is-red');
    const redsLength = $tableReds.length;

    const $tableNorGreensNorReds = $('#result > tbody > tr:not(.is-green):not(.is-red)');
    const norGreensNorRedsLength = $tableNorGreensNorReds.length;
    // console.error($tableReds, $tableGreens, $tableNorGreensNorReds);

    const tableFilterBtnGroup = document.querySelectorAll('.table-filter.btn-group > button');

    tableFilterBtnGroup.forEach((btn, idx) => {
        btn.removeAttribute('disabled');

        btn.addEventListener('click', function() {
        
            switch (idx) {
                case 0:
                    $tableGreens.show();
                    $tableReds.show();
                    $tableNorGreensNorReds.show();
                    document.querySelector('#nElements').textContent = document.querySelector('#totalElements').textContent;                   
                    break;

                case 1:     // Filter Greens
                    $tableGreens.show();
                    $tableReds.hide();
                    $tableNorGreensNorReds.hide();
                    document.querySelector('#nElements').textContent = greensLength;                   
                    break;

                case 2:     // Filter Reds
                    $tableReds.show();
                    $tableGreens.hide();
                    $tableNorGreensNorReds.hide();
                    document.querySelector('#nElements').textContent = redsLength;                   
                    break;

                case 3:     // Filter NOT Reds and NOT Greens
                    $tableNorGreensNorReds.show();
                    $tableGreens.hide();
                    $tableReds.hide();
                    document.querySelector('#nElements').textContent = norGreensNorRedsLength;                   
                    break;
            
                default:
                    break;
            }
        });
    });
}

saveFile = () => {
    // --------------------------------------------------------------------------------
    // StreamSaver.js plug-in in here: https://github.com/jimmywarting/StreamSaver.js
    // Mind you some index.html scripts are loaded by now...
    // --------------------------------------------------------------------------------
    // A simple one (works very well with Apps with NPM dependencies) could be FileSaver.js => https://github.com/eligrey/FileSaver.js/

    let fileContent = '';
    const br = '\r\n';
    const tableLinesContent = document.querySelectorAll('#result > tbody > tr');
    tableLinesContent.forEach(line => {
        const eachLineCell = [].slice.call(line.children);
        fileContent += `${eachLineCell[0].textContent}\r\n${eachLineCell[1].textContent} --> ${eachLineCell[2].textContent}\r\n${eachLineCell[4].textContent}\r\n\r\n`;
    })


    const blob = new Blob([fileContent])
    const fileStream = streamSaver.createWriteStream('transl.srt', {
      size: blob.size // Makes the procentage visiable in the download
    })

    // One quick alternetive way if you don't want the hole blob.js thing:
    // const readableStream = new Response(
    //   Blob || String || ArrayBuffer || ArrayBufferView
    // ).body
    const readableStream = blob.stream()

    // more optimized pipe version
    // (Safari may have pipeTo but it's useless without the WritableStream)
    if (window.WritableStream && readableStream.pipeTo) {
        return readableStream.pipeTo(fileStream)
            .then(() => {
                // console.log('done writing');
            })
        ;
    }

    // // Write (pipe) manually
    // window.writer = fileStream.getWriter()

    // const reader = readableStream.getReader()
    // const pump = () => reader.read()
    //   .then(res => res.done
    //     ? writer.close()
    //     : writer.write(res.value).then(pump))

    // pump()
}

toggleLoadingWheel = () => {
    const loadingWheels = document.querySelectorAll('div.headers-preloader');
    loadingWheels.forEach(eachOne => eachOne.style.display = eachOne.style.display === 'flex' ? 'none' : 'flex');

}
